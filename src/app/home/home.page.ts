import { Platform } from '@ionic/angular';
import { Component } from '@angular/core';
// import * as VoIPPushNotification from "cordova-ios-voip-push";
// require 'cordova-ios-voip-push';

declare var cordova: any;
// declare var VoIPPushNotification: any;
declare let VoipPush: any;
// declare let CordovaCall: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private platform:Platform) {
    this.platform.ready().then(() => {
      let push = VoipPush.init();
      push.on('registration', function(data) {
        console.log("[Ionic] registration callback called");
        console.log(data);
    
        //data.deviceToken;
        //do something with the device token (probably save it to your backend service)
      });
      
      push.on('notification', function(data) {
          console.log("[Ionic] notification callback called");
          console.log('data: ');
          console.log(data);
      
          // do something based on received data
          cordova.plugins.CordovaCall.receiveCall(data.aps.alert);
      });
      
      push.on('error', function(e) {
          console.log(e);
      });
    })
    // let push = cordova.plugins.VoIPPushNotification.init();
  }

  receiveCall() {
    setTimeout(() => {
      cordova.plugins.CordovaCall.receiveCall("atmo");
      // CordovaCall.receiveCall("atmo");
    }, 1000);
  }
}
